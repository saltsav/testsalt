﻿using System;
using Assets.Project.Scripts;
using UnityEngine;

public class SwipeDetector: MonoBehaviour {
    private Vector2 fingerDownPosition;
    private Vector2 fingerUpPosition;

    [SerializeField]
    private bool detectSwipeOnlyAfterRelease = false;

    [SerializeField]
    private float minDistanceForSwipe = 20f;

    public static event Action<SwipeData> OnSwipe = delegate { };

    private void Update () {
        if (Input.touchSupported) {
            foreach(var touch in Input.touches) {
                if(touch.phase == TouchPhase.Began) {
                    fingerUpPosition = touch.position;
                    fingerDownPosition = touch.position;
                }

                if(!detectSwipeOnlyAfterRelease && touch.phase == TouchPhase.Moved) {
                    fingerDownPosition = touch.position;
                    DetectSwipe();
                }

                if(touch.phase == TouchPhase.Ended) {
                    fingerDownPosition = touch.position;
                    DetectSwipe();
                }
            }
        } else {

        }
        
    }

    private void DetectSwipe () {
        if(SwipeDistanceCheckMet()) {
            if(IsVerticalSwipe()) {
                var direction = fingerDownPosition.y - fingerUpPosition.y > 0 ? SwipeDirection.Down : SwipeDirection.Up;
                SendSwipe(direction);
            } else {
                var direction = fingerDownPosition.x - fingerUpPosition.x > 0 ? SwipeDirection.Left : SwipeDirection.Right;
                SendSwipe(direction);
            }
            fingerUpPosition = fingerDownPosition;
        }
    }

    private bool IsVerticalSwipe () {
        return VerticalMovementDistance() > HorizontalMovementDistance();
    }

    private bool SwipeDistanceCheckMet () {
        return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
    }

    private float VerticalMovementDistance () {
        return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
    }

    private float HorizontalMovementDistance () {
        return Mathf.Abs(fingerDownPosition.x - fingerUpPosition.x);
    }

    private void SendSwipe (SwipeDirection direction) {
        SwipeData swipeData = new SwipeData() {
            Direction = direction,
            StartPosition = fingerDownPosition,
            EndPosition = fingerUpPosition
        };
        OnSwipe(swipeData);
    }
}

public struct SwipeData {
    public Vector2 StartPosition;
    public Vector2 EndPosition;
    public SwipeDirection Direction;
}

