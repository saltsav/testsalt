﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Project.Scripts {
    public class CellOnView : MonoBehaviour, IPointerClickHandler {
        public CellMaze cellMaze;
        public TextMesh textMesh;
        public int index;
        public MeshRenderer meshRenderer;

        public void Setup() {
            meshRenderer = GetComponent<MeshRenderer>();
            textMesh = GetComponentInChildren<TextMesh>();
        }

        public void OnPointerClick(PointerEventData eventData) {
            //FindPath.inst.FindPathToThisCell(cellMaze);
        }

        public void SetMat(Material mat) {
            meshRenderer.material = mat;
        }
    }
}