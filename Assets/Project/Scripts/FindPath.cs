﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class FindPath: MonoBehaviour {
        public static FindPath inst;
        public CellMaze startCell;
        public CellMaze finalCell;
        public List<CellMaze> listFirstWave = new List<CellMaze>();
        public List<CellMaze> listSecondWave = new List<CellMaze>();
        private List<CellMaze> reversPath = new List<CellMaze>();

        public void Awake () {
            inst = this;
        }

        public IEnumerator IEnumWaitTest () {
            while(true) {
                yield return null;
                if(Input.GetKeyUp(KeyCode.A)) {
                    break;
                }
            }
        }

        /*public void FindPathToThisCell (CellMaze cellMaze) {
            PlayerManager.inst.StopMoveForId(PlayerManager.inst.GetPlayerId());
            StartCoroutine(IEnumFindToThisCell(cellMaze));
        }*/

        public List<CellMaze> FindToThisCell (CellMaze startCell, CellMaze finalCell) {
            ResetAllMass();

            var finalPointIsVisited = false;
            int massPath = 0;
            listFirstWave.Clear();
            listSecondWave.Clear();
            listSecondWave.Add(startCell);
            SetRoundRoad(startCell);
            int flagNumberWave = 1;
            do {
                if(flagNumberWave == 1) {
                    listFirstWave.Clear();
                } else {
                    listSecondWave.Clear();
                }

                foreach(var cellRound in flagNumberWave == 1 ? listSecondWave : listFirstWave) {
                    if(!cellRound.isVisited) {
                        cellRound.isVisited = true;
                        cellRound.numberMassPath = massPath;
                        if(cellRound.i == finalCell.i && cellRound.j == finalCell.j) {
                            finalPointIsVisited = true;
                        }
                        foreach(var cellRoad in cellRound.cellsRoundRoad) {
                            cellRoad.beforRoad = cellRound;
                            if(flagNumberWave == 1) {
                                listFirstWave.Add(cellRoad);
                            } else {
                                listSecondWave.Add(cellRoad);
                            }
                        }
                    }
                }

                foreach(var cellRound in flagNumberWave == 1 ? listFirstWave : listSecondWave) {
                    SetRoundRoad(cellRound);
                }

                massPath++;
                if (massPath > GP.maxPathMove) {
                    finalCell = null;
                    break;
                }
                if(flagNumberWave == 1) {
                    flagNumberWave = 2;
                } else {
                    flagNumberWave = 1;
                }
            } while(!finalPointIsVisited);

            if (finalCell?.beforRoad != null) {
                reversPath.Clear();
                reversPath.Add(finalCell);
                var cell = finalCell.beforRoad;
                do {

                    reversPath.Add(cell);
                    cell = cell.beforRoad;
                    if(cell == null || cell.numberMassPath == 0) {
                        break;
                    }
                } while(true);

                return reversPath;
            }

            return null;
        }



        public void SetRoundRoad (CellMaze cellMaze) {
            cellMaze.cellsRoundRoad.Clear();
            if(cellMaze.i + 1 < GP.sizeMaze && !GenerationMaze.inst.mazeArray[cellMaze.i + 1, cellMaze.j].isVisited && GenerationMaze.inst.mazeArray[cellMaze.i + 1, cellMaze.j].typeCell == TypeCell.road) {
                cellMaze.cellsRoundRoad.Add(GenerationMaze.inst.mazeArray[cellMaze.i + 1, cellMaze.j]);
            }
            if(cellMaze.i - 1 > 0 && !GenerationMaze.inst.mazeArray[cellMaze.i - 1, cellMaze.j].isVisited && GenerationMaze.inst.mazeArray[cellMaze.i - 1, cellMaze.j].typeCell == TypeCell.road) {
                cellMaze.cellsRoundRoad.Add(GenerationMaze.inst.mazeArray[cellMaze.i - 1, cellMaze.j]);
            }
            if(cellMaze.j + 1 < GP.sizeMaze && !GenerationMaze.inst.mazeArray[cellMaze.i, cellMaze.j + 1].isVisited && GenerationMaze.inst.mazeArray[cellMaze.i, cellMaze.j + 1].typeCell == TypeCell.road) {
                cellMaze.cellsRoundRoad.Add(GenerationMaze.inst.mazeArray[cellMaze.i, cellMaze.j + 1]);
            }
            if(cellMaze.j - 1 > 0 && !GenerationMaze.inst.mazeArray[cellMaze.i, cellMaze.j - 1].isVisited && GenerationMaze.inst.mazeArray[cellMaze.i, cellMaze.j - 1].typeCell == TypeCell.road) {
                cellMaze.cellsRoundRoad.Add(GenerationMaze.inst.mazeArray[cellMaze.i, cellMaze.j - 1]);
            }

        }

        [EasyButtons.Button]
        public void ResetAllMass () {
            for(var i = 0; i < GP.sizeMaze; i++) {
                for(var j = 0; j < GP.sizeMaze; j++) {
                    if(GenerationMaze.inst.mazeArray[i, j].typeCell == TypeCell.road) {
                        GenerationMaze.inst.mazeArray[i, j].numberMassPath = 0;
                        GenerationMaze.inst.mazeArray[i, j].isVisited = false;
                        GenerationMaze.inst.mazeArray[i, j].beforRoad = null;
                    }
                }
            }

            //ViewMaze.inst.ResetText();
        }


        public class OnePathBlock {
            public int i;
        }
    }
}