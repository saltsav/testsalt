﻿using UnityEngine;

namespace Assets.Project.Scripts {
    public class SpawnController : MonoBehaviour {
        public static SpawnController inst;
        public Transform parentForControllers;

        private void Awake() {
            if (inst != null) {
                Destroy(gameObject);
            } else {
                DontDestroyOnLoad(gameObject);
                inst = this;
            }   
        }

        public T InstController<T> (string typeController) where T : MonoBehaviour{
            var res = Resources.Load("Controllers/" + typeController, typeof(GameObject)) as GameObject;
            if(res != null) {
                var controller = Instantiate(res);
                if (controller != null) {
                    controller.name = typeController;
                    controller.gameObject.SetActive(true);
                    if (parentForControllers) {
                        controller.transform.SetParent(parentForControllers);
                    }
                    return controller.GetComponent<T>();
                }
            } else {
                var go = new GameObject();
                var controller = go.AddComponent<T>();
                if(controller != null) {
                    controller.name = typeController;
                    controller.gameObject.SetActive(true);
                    if(parentForControllers) {
                        controller.transform.SetParent(parentForControllers);
                    }
                    return controller.GetComponent<T>();
                }
            }
            Debug.LogError("Resources => " + typeController + " <= dont find");
            return default(T);
        }
    }
}

