﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Project.Scripts {
    public enum State {
        move,
        idle
    }
    public class BasePlayer: MonoBehaviour {
        private string id;
        public CellMaze cellMaze;
        public List<Coroutine> listCoroMove = new List<Coroutine>();
        public Text text;
        public InfoByOnePlayer infoByOnePlayer = new InfoByOnePlayer();
        public bool canStop;
        public State state = State.idle;

        public float time = 1f;
        public float currentTime;
        public bool needSend;

        public string GetId() {
            return id;
        }

        public void Setup(CellMaze cellMazeInfo, string newId) {
            id = newId;
            infoByOnePlayer.id = newId;
            infoByOnePlayer.wrapperCellMaze.i = cellMazeInfo.i;
            infoByOnePlayer.wrapperCellMaze.j = cellMazeInfo.j;
            infoByOnePlayer.chatStr = "";
            if (id == GameInfo.inst.myID) {
                PlayerManager.inst.StartMovePlayerByListAndId(new List<CellMaze>() { cellMazeInfo }, id);
            }

            //StartCoroutine(IEnumSetInfoCellMazePlayer());
        }

        public void SetChatText(string str) {
            text.text = str;
            infoByOnePlayer.chatStr = str;
        }

        /*public void MoveToThisPoint (SwipeDirection swipeDirection) {
            var newCellMaze = GenerationMaze.inst.GetCellMazeBySwipeDirection(swipeDirection, cellMaze);
            if(newCellMaze != null) {
                cellMaze = newCellMaze;
                transform.localPosition = new Vector3(cellMaze.i, cellMaze.j, GP.zMaze);
            }
        }*/

        [EasyButtons.Button]
        public void StopCoroMove () {
            foreach (var coro in listCoroMove) {
                if (coro != null) {
                    StopCoroutine(coro);
                }
            }

            listCoroMove.Clear();
        }

        public void StartMoveByPath (List<CellMaze> listPath, bool needFind) {
            //Debug.LogError(listPath[listPath.Count - 1].i + " " + listPath[listPath.Count - 1].j);
            StopCoroMove();
            if (needFind) {
                listCoroMove.Add(StartCoroutine(IEnumMove(FindPath.inst.FindToThisCell(cellMaze, listPath[listPath.Count - 1]))));
            } else {
                listCoroMove.Add(StartCoroutine(IEnumMove(listPath)));
            }
          
        }

        public IEnumerator IEnumMove (List<CellMaze> listPath) {
            if (listPath == null) {
                yield break;
            }
            state = State.move;
            for(var i = listPath.Count - 1; i >= 0; i--) {
                cellMaze = listPath[i];
                infoByOnePlayer.wrapperCellMaze.i = cellMaze.i;
                infoByOnePlayer.wrapperCellMaze.j = cellMaze.j;
                canStop = false;
                var coro = StartCoroutine (MainAnimator.inst.IEnumMoveTransform(transform, transform.localPosition, new Vector3(cellMaze.i, cellMaze.j, GP.zMaze), 1 / GP.moveSpeed));
                listCoroMove.Add(coro);
                yield return coro;
                canStop = true;
            }

            if (id == GameInfo.inst.myID) {
                needSend = true;
            }
            state = State.idle;
        }

        /*[EasyButtons.Button]
        public IEnumerator  IEnumSetInfoCellMazePlayer () {
            while (id == GameInfo.inst.myID) {
                if (currentTime > 0) {
                    currentTime -= Time.deltaTime;
                }

                if (currentTime <= 0 && needSend) {
                    needSend = false;
                    currentTime = time;
                    var data = JsonConvert.SerializeObject(infoByOnePlayer.wrapperCellMaze);
                    GSAPI.SetInfoCellMazePlayer(data);
                }

                yield return null;
            }
           
        }*/


        /*[EasyButtons.Button]
        public void MoveUp () {
            MoveToThisPoint(SwipeDirection.Up);
        }

        [EasyButtons.Button]
        public void MoveDown () {
            MoveToThisPoint(SwipeDirection.Down);
        }

        [EasyButtons.Button]
        public void MoveLeft () {
            MoveToThisPoint(SwipeDirection.Left);
        }

        [EasyButtons.Button]
        public void MoveRight () {
            MoveToThisPoint(SwipeDirection.Right);
        }*/

    }

    [Serializable]
    public class InfoByOnePlayer {
        public string id;
        public WrapperCellMaze wrapperCellMaze = new WrapperCellMaze();
        public string chatStr;
    }
}
