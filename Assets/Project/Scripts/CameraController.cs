﻿using UnityEngine;

namespace Assets.Project.Scripts {
    public class CameraController : MonoBehaviour {
        public static CameraController inst;
        public Camera cameraUI;
        public Camera cameraGame;
        private void Awake() {
            inst = this;
        }

        public void SetPositionForCamera(Vector3 v3) {
            cameraGame.transform.position = v3 + Vector3.back*10;
        }

        public void SetParentCamera(Transform tran) {
            cameraGame.transform.SetParent(tran);
            cameraGame.transform.localPosition = Vector3.back * 20;
        }

        public void SetGameCamera(bool var) {
            cameraUI.gameObject.SetActive(!var);
            cameraGame.gameObject.SetActive(var);
        }
    }
}