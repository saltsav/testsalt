﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class CommandType {
        public const string moveListPath = "moveListPath";
        public const string chatSend = "chatSend";
        public const string infoRoom = "infoRoom";
    }

    public class BaseJsonInfo {
        public string type;
        public Data data;
    }

    public class Data {

    }

    [Serializable]
    public class MoveListPath : Data {
        public string id;
        public List<WrapperCellMaze> listPath = new List<WrapperCellMaze>();
    }

    [Serializable]
    public class ChatSend: Data {
        public string id;
        public string chat;
    }

    [Serializable]
    public class InfoRoom: Data {
        public string numberRoom;
        public string mazeRoom;
        public bool isRoomFull;
    }

    public class GameJsonClasses {
        static GameJsonClasses () {
            GSAPI.CallbackSendDataFromClient += CallbackSendDataFromClient;
        }

        public static JsonConverter[] _converters = {
            new CommandTypeConverter(),
        };

        public static Action<MoveListPath> actionMoveListPath;
        public static Action<ChatSend> actionChatSend;
        public static Action<InfoRoom> actionInfoRoom;

        public static void CallbackSendDataFromClient (string str) {
            var data = JsonConvert.DeserializeObject<BaseJsonInfo>(str, new JsonSerializerSettings() { Converters = _converters });
            switch (data.type) {
                case CommandType.moveListPath:
                    if (actionMoveListPath != null) {
                        actionMoveListPath((MoveListPath)data.data);
                    }
                    break;
                case CommandType.chatSend:
                    if(actionChatSend != null) {
                        actionChatSend((ChatSend)data.data);
                    }
                    break;
                case CommandType.infoRoom:
                    if(actionInfoRoom != null) {
                        actionInfoRoom((InfoRoom)data.data);
                    }
                    break;
            }
        }
    }

    public class CommandTypeConverter: JsonConverter {
        public override bool CanConvert (Type objectType) {
            return objectType == typeof(BaseJsonInfo);
        }

        public override bool CanWrite {
            get { return false; }
        }

        public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer) {
            throw new NotImplementedException();
        }

        public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            var jsonObject = JObject.Load(reader);
            var obj = new BaseJsonInfo();
            //var obj = default(ServerResponseEvent);
            var valueType = jsonObject["type"].Value<string>();
            switch(valueType) {
                case CommandType.moveListPath:
                    obj.data = new MoveListPath();
                    break;
                case CommandType.chatSend:
                    obj.data = new ChatSend();
                    break;
                case CommandType.infoRoom:
                    obj.data = new InfoRoom();
                    break;
                default:
                    Debug.Log("ERROR! Not found ServerResponseEvent " + valueType);
                    break;
            }
            serializer.Populate(jsonObject.CreateReader(), obj);
            return obj;
        }

    }
}