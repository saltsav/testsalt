﻿using UnityEngine;

namespace Assets.Project.Scripts {
    public class GameInfo : Singleton<GameInfo> {
        [SerializeField] private string _myID;
        public string myID {
            get { return _myID; }
            set { _myID = value; }
        }

        public bool isRegistration;
        public bool isPlayerConnected;

        [EasyButtons.Button]
        public void StartFromController() {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            GSAPI.CallbackRegistrationDevice += CallbackRegistration;
            GSAPI.RegistrationDevice(SystemInfo.deviceName);
        }


        public void CallbackRegistration (string id) {
            if (id != "") {
                isRegistration = true;
                myID = id;
                //PlayerController.inst.myPlayer.playerId = id;
                ConnectPlayer();
            } else {
                isRegistration = false;
            }
         
        }

        public string GetIdPlayer() {
            return myID;
        }

        public void ConnectPlayer () {
            GSAPI.actionConnectPlayer += (var) => {
                if(var) {
                    isPlayerConnected = true;
                } else {
                    Debug.LogError("actionConnectPlayer");
                }
            };
            GSAPI.ConnectPlayer();
        }
    }
}