﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class PlayerManager : MonoBehaviour {
        public static PlayerManager inst;
        public List<BasePlayer> listBasePlayers;

        [ReadOnly]public BasePlayer mainPlayer;
        public BasePlayer prefabBasePlayer;

       // public InputField inputField;
        public string chatPlayer;
        public bool needSendChatPlayer;

        public AllPlayersInfo test;

        public CellMaze cellToMove;
         
        private float testTime = 0.2f;
        public float currentTime;

        public List<Coroutine> listCoroutines = new List<Coroutine>();

        public void Awake() {
            inst = this;
        }

        public void StartFromController (string id) {
            GameJsonClasses.actionMoveListPath += StartMovePlayerByListAndIdFromServer;
            GameJsonClasses.actionChatSend += ChatFromServer;
            SetupPlayer(id);
            listCoroutines.Add(StartCoroutine(IEnumSendChat()));
            /*inputField.onValueChanged.AddListener((str) => { chatPlayer = str;
                needSendChatPlayer = true;
            });*/
            //needSendChatPlayer = true;
            GSAPI.actionPlayerLeave += PlayerLeave;

            GSAPI.actionGetPlayersInfo += GetPlayersInfo;
            GSAPI.GetPlayersInfo();

            listCoroutines.Add(StartCoroutine(IEnumMove()));
        }

        public void StopFromController() {
            GameJsonClasses.actionMoveListPath -= StartMovePlayerByListAndIdFromServer;
            GameJsonClasses.actionChatSend -= ChatFromServer;
            GSAPI.actionPlayerLeave -= PlayerLeave;
            ClearAllPlayer();
            StopCoroutineAll();
        }

        public IEnumerator IEnumMove() {
            while (true) {
                if (mainPlayer.state != State.idle) {
                    yield return null;
                    continue;
                }

                cellToMove = null;
                var i = mainPlayer.cellMaze.i;
                var j = mainPlayer.cellMaze.j;

                switch(JoystickController.inst.finalArrow) {
                    case FinalArrow.none:
                        break;
                    case FinalArrow.zone1:
                        if(GenerationMaze.inst.mazeArray[i, j + 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j + 1];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i + 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i + 1, j];
                        }
                        break;
                    case FinalArrow.zone2:
                        if(GenerationMaze.inst.mazeArray[i + 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i + 1, j];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i, j + 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j + 1];
                        }
                        break;
                    case FinalArrow.zone3:
                        if(GenerationMaze.inst.mazeArray[i + 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i + 1, j];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i, j - 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j - 1];
                        }
                        break;
                    case FinalArrow.zone4:
                        if(GenerationMaze.inst.mazeArray[i, j - 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j - 1];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i + 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i + 1, j];
                        }
                        break;
                    case FinalArrow.zone5:
                        if(GenerationMaze.inst.mazeArray[i, j - 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j - 1];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i - 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i - 1, j];
                        }
                        break;
                    case FinalArrow.zone6:
                        if(GenerationMaze.inst.mazeArray[i - 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i - 1, j];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i, j - 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j - 1];
                        }
                        break;
                    case FinalArrow.zone7:
                        if(GenerationMaze.inst.mazeArray[i - 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i - 1, j];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i, j + 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j + 1];
                        }
                        break;
                    case FinalArrow.zone8:
                        if(GenerationMaze.inst.mazeArray[i, j + 1].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i, j + 1];
                            break;
                        }
                        if(GenerationMaze.inst.mazeArray[i - 1, j].typeCell == TypeCell.road) {
                            cellToMove = GenerationMaze.inst.mazeArray[i - 1, j];
                        }
                        break;
                }

                if(cellToMove != null) {
                    StartMovePlayerByListAndId(new List<CellMaze>() { cellToMove }, GameInfo.inst.myID);
                    //FindPath.inst.FindPathToThisCell(cellToMove);
                }

                yield return null;
            }
        }

        public void GetPlayersInfo(string data) {
            GSAPI.actionGetPlayersInfo = GetPlayersInfo;
            //Debug.Log(data);
            var allPlayersInfo = JsonConvert.DeserializeObject<AllPlayersInfo>(data);
            test = allPlayersInfo;


            foreach (var moveListPath in allPlayersInfo.listMoveListPath) {
                StartMovePlayerByListAndIdFromServer(moveListPath);
            }

            foreach(var listId in allPlayersInfo.listId) {
                ChatFromServer(listId);
            }
        }

        public void PlayerLeave (string data) {
            Debug.Log(data);
            for (int i = 0; i < listBasePlayers.Count; i++) {
                if (listBasePlayers[i].GetId() == data) {
                    Destroy(listBasePlayers[i].gameObject);
                    listBasePlayers.RemoveAt(i);
                    break;
                }
            }
        }



        public IEnumerator IEnumSendChat () {
            while(true) {
                if (needSendChatPlayer) {
                    needSendChatPlayer = false;
                    var baseJsonInfo = new BaseJsonInfo();
                    baseJsonInfo.type = CommandType.chatSend;
                    baseJsonInfo.data = new ChatSend() {
                        id = GetPlayerId(),
                        chat = chatPlayer,

                    };
                    var data = JsonConvert.SerializeObject(baseJsonInfo);
                    GSAPI.SendDataFromClient(data, LobbyController.inst.numberRoom);

                    GSAPI.SetInfoChatPlayer(chatPlayer);
                }
                yield return new WaitForSeconds(0.5f);
            }
        }

        public BasePlayer GetPlayer () {
            return mainPlayer; 
        }
        
        public void SetupPlayer(string id, CellMaze cellMaze = null) {
            var pl = Instantiate(prefabBasePlayer);
            listBasePlayers.Add(pl);
            pl.name = "pl_" + id;
            if (id == GetPlayerId()) {
                mainPlayer = pl;
                var emptyCellMaze = GenerationMaze.inst.GetEmptyCellMaze();
                //Debug.Log("Player " + id  + " on " + emptyCellMaze.i + " " + emptyCellMaze.j);
                mainPlayer.transform.localPosition = new Vector3(emptyCellMaze.i, emptyCellMaze.j, GP.zMaze);
                mainPlayer.cellMaze = emptyCellMaze;
                CameraController.inst.SetParentCamera(mainPlayer.transform);
                mainPlayer.Setup(emptyCellMaze, id);
            } else {
                Debug.Log("Player " + id + " on " + cellMaze.i + " " + cellMaze.j);
                pl.transform.localPosition = new Vector3(cellMaze.i, cellMaze.j, GP.zMaze);
                pl.cellMaze = cellMaze;
                pl.Setup(cellMaze, id);
            }
           
        }

        public string GetPlayerId() {
            return GameInfo.inst.GetIdPlayer();
        }

        public void StopMoveForId(string id) {
            GetPlayerById(id).StopCoroMove();
        }

        public void Update() {
            if (currentTime > 0) {
                currentTime -= Time.unscaledDeltaTime;
            } else {
                if(lastListPathToSend != null) {
                    //Debug.LogError("SEND REM");
                    SendListPath(lastListPathToSend);
                    lastListPathToSend = null;
                }
            }
        }

        private List<CellMaze> lastListPathToSend = null;

        public void StartMovePlayerByListAndId (List<CellMaze> listPath, string id) {
          
            var player = GetPlayerById(id);
            player.StartMoveByPath(listPath, false);

            if(currentTime > 0) {
                //Debug.LogError("REME " + listPath[listPath.Count - 1].i + " " + listPath[listPath.Count - 1].j);
                lastListPathToSend = listPath;
                return;
            }

            currentTime = testTime;
            SendListPath(listPath);
            lastListPathToSend = null;
        }

        public void SendListPath (List<CellMaze> listPath) {
            var baseJsonInfo = new BaseJsonInfo();
            baseJsonInfo.type = CommandType.moveListPath;
            baseJsonInfo.data = new MoveListPath() {
                id = GetPlayerId(),
                listPath = GenerationMaze.ConvertToListWrapperCellMaze(listPath),
            };
            var data = JsonConvert.SerializeObject(baseJsonInfo);
            GSAPI.SendDataFromClient(data, LobbyController.inst.numberRoom);
        }

        public void StartMovePlayerByListAndIdFromServer (MoveListPath moveListPath) {
            if (moveListPath.id == GetPlayerId()) {
                return;
            }
            var player = GetPlayerById(moveListPath.id);
            if(player == null) {
                Debug.Log("null");
                SetupPlayer(moveListPath.id, GenerationMaze.ConvertToListCellMaze(moveListPath.listPath)[0]);
            }

            if (player != null) {
                player.StartMoveByPath(GenerationMaze.ConvertToListCellMaze(moveListPath.listPath), true);
            }

        }

        public void ChatFromServer(ChatSend chatSend) {
            var player = GetPlayerById(chatSend.id);
            player.SetChatText(chatSend.chat);
        }

        public BasePlayer GetPlayerById(string id) {
            foreach (var bp in listBasePlayers) {
                if (bp.GetId() == id) {
                    return bp;
                }
            }
            return null;
        }

        public void ClearAllPlayer () {
            CameraController.inst.SetParentCamera(null);
            foreach(var bp in listBasePlayers) {
                if(bp) {
                    Destroy(bp.gameObject);
                }
            }
            listBasePlayers.Clear();
        }

        public void StopCoroutineAll () {
            foreach(var coro in listCoroutines) {
                if(coro != null) {
                    StopCoroutine(coro);
                }
            }
            listCoroutines.Clear();
        }
    }

    [Serializable]
    public class AllPlayersInfo {
        public List<ChatSend> listId = new List<ChatSend>();
        public List<MoveListPath> listMoveListPath = new List<MoveListPath>();
    }
}
/*[EasyButtons.Button]
public void TEST () {
    var allPlayersInfo = new AllPlayersInfo();
    allPlayersInfo.listMoveListPath.Add(new MoveListPath() {
        id = "333",
        listPath = new List<WrapperCellMaze>() {
            new WrapperCellMaze(){i =11, j = 22}
        }
    });
    allPlayersInfo.listMoveListPath.Add(new MoveListPath() {
        id = "222",
        listPath = new List<WrapperCellMaze>() {
            new WrapperCellMaze(){i = 22, j = 44}
        }
    });

    allPlayersInfo.listId.Add(new ChatSend() {
        id = "333",
        chat = "sdsdg"
    });
    allPlayersInfo.listId.Add(new ChatSend() {
        id = "11",
        chat = "22"
    });


    var strrrr = JsonConvert.SerializeObject(allPlayersInfo);
    Debug.Log(strrrr);
}*/

/* [EasyButtons.Button]
 public void Test2() {
     var aff = JsonConvert.DeserializeObject<AllPlayersInfo>(strrrr);
     Debug.Log(aff);
 }*/
