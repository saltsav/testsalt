﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Project.Scripts {
    public enum SwipeDirection {
        Up,
        Down,
        Left,
        Right
    }

    public class MoveController : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {
        public static MoveController inst;
        public Vector2 fingerDownPosition;
        public Vector2 fingerUpPosition;
        public bool isDrag;

        public float minDistanceForSwipe = 20f;

        public void Awake() {
            inst = this;
        }

        public void OnDrag(PointerEventData eventData) {
            isDrag = true;
        }

        public void DetectedSwipeType() {
            Debug.Log(fingerDownPosition + " " + fingerUpPosition);
            if(SwipeDistanceCheckMet()) {
                if(IsVerticalSwipe()) {
                    var direction = fingerDownPosition.y - fingerUpPosition.y < 0 ? SwipeDirection.Up : SwipeDirection.Down;
                    Debug.Log(direction);
                } else {
                    var direction = fingerDownPosition.x - fingerUpPosition.x < 0 ? SwipeDirection.Right : SwipeDirection.Left;
                    Debug.Log(direction);
                }
                fingerUpPosition = fingerDownPosition;
            }
        }

        private bool IsVerticalSwipe () {
            return VerticalMovementDistance() > HorizontalMovementDistance();
        }

        private bool SwipeDistanceCheckMet () {
            return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
        }

        private float VerticalMovementDistance () {
            return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
        }

        private float HorizontalMovementDistance () {
            return Mathf.Abs(fingerDownPosition.x - fingerUpPosition.x);
        }


        public void OnPointerUp(PointerEventData eventData) {
            fingerUpPosition = eventData.position;
            if(isDrag) {
                DetectedSwipeType();
            }
            isDrag = false;
        }

        public void OnPointerDown(PointerEventData eventData) {
            fingerDownPosition = eventData.position;
        }


    }
}

