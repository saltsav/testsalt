﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Project.Scripts {
    public class MainAnimator : Singleton<MainAnimator> {

        public IEnumerator IEnumMoveTransform (Transform tran, Vector3 startPos, Vector3 finalPos, float time) {
            float currentTime = 0;

            while(currentTime < time) {
                var t = currentTime / time;
                tran.position = Vector3.Lerp(startPos, finalPos, t);
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }

            tran.position = finalPos;

        }

        public IEnumerator IEnumChangeScalesOnTransform (Transform rt, Vector3 finalScale, float time, float pow) {
            Vector3 startScale = rt.localScale;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                rt.localScale = Vector2.Lerp(startScale, finalScale, Mathf.Pow(t, pow));
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            rt.localScale = finalScale;
        }

        public IEnumerator IEnumMoveLocalPosOnRectTransform (RectTransform rt, Vector2 finalPos, float time, float pow, Action action = null, bool? boolStop = null, float? stopTime = null, bool useUnscaledDeltaTime = true) {
            Vector2 startPos = rt.localPosition;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                rt.anchoredPosition = Vector2.Lerp(startPos, finalPos, Mathf.Pow(t, pow));
                if (useUnscaledDeltaTime) {
                    currentTime += Time.unscaledDeltaTime;
                } else {
                    currentTime += Time.deltaTime;
                }
               
                if (action != null) {
                    action.Invoke();
                }

                if (boolStop != null && (bool)boolStop) {
                    if (stopTime!= null && currentTime > (float)stopTime) {
                       break;
                    }
                }
                yield return null;
            }

            if (boolStop == null) {
                rt.anchoredPosition = finalPos;
            }

            yield return null;
        }

        public void ImmediateMoveLocalPosOnRectTransform (RectTransform rt, Vector2 finalPos) {
             rt.anchoredPosition = finalPos;
        }

        public IEnumerator IEnumMoveLocalPosOnTransform (Transform rt, Vector3 finalPos, float time, float pow) {
            Vector3 startPos = rt.localPosition;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                rt.localPosition = Vector3.Lerp(startPos, finalPos, Mathf.Pow(t, pow));
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            rt.localPosition = finalPos;
        }

        public IEnumerator IEnumMoveResouresOnCameraParent (Transform rt, Transform finalRt, float time, float pow) {
            Vector3 startPos = rt.localPosition;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                var finalPos = new Vector3(finalRt.localPosition.x, finalRt.localPosition.y, startPos.z);
                rt.localPosition = Vector3.Lerp(startPos, finalPos, Mathf.Pow(t, pow));
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            rt.position = new Vector3(finalRt.localPosition.x, finalRt.localPosition.y, startPos.z);
        }

        public IEnumerator IEnumMoveTransToTrans (Transform startRt, Transform finalRt, float time, float pow) {
            Vector3 startPos = startRt.position;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                var finalPos = new Vector3(finalRt.position.x, finalRt.position.y, startPos.z);
                startRt.localPosition = Vector3.Lerp(startPos, finalPos, Mathf.Pow(t, pow));
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            startRt.position = new Vector3(finalRt.position.x, finalRt.position.y, startPos.z);
        }

        public IEnumerator IEnumMoveRectTransformoffsetMax (RectTransform rt, Vector2 finalPos, float time, float pow) {
            Vector2 startPos = rt.offsetMax;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                rt.offsetMax = Vector2.Lerp(startPos, finalPos, Mathf.Pow(t, pow));
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            rt.offsetMax = finalPos;
        }

        public IEnumerator IEnumChangeAlfaOnText (Text text, float finalAlfa, float time, float pow) {
            var currentColor = text.color;
            float startAlfa = text.color.a;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;

                var curA = Mathf.Lerp(startAlfa, finalAlfa, Mathf.Pow(t, pow));
                text.color = new Color(currentColor.r, currentColor.g, currentColor.b, curA);
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            text.color = new Color(currentColor.r, currentColor.g, currentColor.b, finalAlfa);
        }

        public IEnumerator IEnumChangeAlfaOnImage (Image img, float finalAlfa, float time, float pow) {
            var currentColor = img.color;
            float startAlfa = img.color.a;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;

                var curA = Mathf.Lerp(startAlfa, finalAlfa, Mathf.Pow(t, pow));
                img.color = new Color(currentColor.r, currentColor.g, currentColor.b, curA);
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            img.color = new Color(currentColor.r, currentColor.g, currentColor.b, finalAlfa);
        }

        public IEnumerator IEnumChangeColorOnImage (Image img, Color finalColor, float time, float pow) {
            var startColor = img.color;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                var curR = Mathf.Lerp(startColor.r, finalColor.r, Mathf.Pow(t, pow));
                var curG = Mathf.Lerp(startColor.g, finalColor.g, Mathf.Pow(t, pow));
                var curB = Mathf.Lerp(startColor.b, finalColor.b, Mathf.Pow(t, pow));
                var curA = Mathf.Lerp(startColor.a, finalColor.a, Mathf.Pow(t, pow));
                img.color = new Color(curR, curG, curB, curA);
                currentTime += Time.deltaTime;
                yield return null;
            }
            img.color = finalColor;
        }

        public IEnumerator IEnumChangeColorOnText (Text text, Color finalColor, float time, float pow) {
            var startColor = text.color;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                var curR = Mathf.Lerp(startColor.r, finalColor.r, Mathf.Pow(t, pow));
                var curG = Mathf.Lerp(startColor.g, finalColor.g, Mathf.Pow(t, pow));
                var curB = Mathf.Lerp(startColor.b, finalColor.b, Mathf.Pow(t, pow));
                var curA = Mathf.Lerp(startColor.a, finalColor.a, Mathf.Pow(t, pow));
                text.color = new Color(curR, curG, curB, curA);
                currentTime += Time.deltaTime;
                yield return null;
            }
            text.color = finalColor;
        }

        public IEnumerator IEnumRotationZOnTransform (Transform rt, float startZ, float finalZ, float time, float pow) {
            //float startZ = rt.localEulerAngles.z > 180 ? rt.localEulerAngles.z - 180 : rt.localEulerAngles.z;
            float currentTime = 0;
            while(currentTime < time) {
                var t = currentTime / time;
                var curZ = Mathf.Lerp(startZ, finalZ, Mathf.Pow(t, pow));
                //Debug.Log(t + " " + curZ + " " + currentTime + " " + time);
                rt.localEulerAngles = new Vector3(rt.localEulerAngles.x, rt.localEulerAngles.y, curZ);
                currentTime += Time.unscaledDeltaTime;
                yield return null;
            }
            rt.localEulerAngles = new Vector3(rt.localEulerAngles.x, rt.localEulerAngles.y, finalZ);
        }


        //public IEnumerator IEnumMoveAnchoredPosition (RectTransform rt, Vector2 finalPos, float time, float pow) {
        // Vector2 startPos = rt.anchoredPosition;
        // float currentTime = 0;
        // while(currentTime < time) {
        //     var t = currentTime / time;
        //     rt.anchoredPosition = Vector2.Lerp(startPos, finalPos, Mathf.Pow(t, pow));
        //     Debug.Log("rt " + rt.anchoredPosition);
        //     currentTime += Time.unscaledDeltaTime;
        //     yield return null;
        // }
        // rt.anchoredPosition = finalPos;
        //}
    }
}