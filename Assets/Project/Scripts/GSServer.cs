﻿using System;
using System.Collections;
using System.Globalization;
using GameSparks.Api.Messages;
using GameSparks.Core;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class RequestData {
        public CallbackData data;
    }

    public class CallbackData {
        public string callback;
    }

    public class InfoToServer {
        public string typeRequest;
        public string strData;
        public string idRoom;
    }

    public class GSServer : MonoBehaviour {
        public static GSServer inst;
        private bool onAuthorizationAvailable;
        private bool onGSAvailable;
        private string responsesUserId;

        private void Awake () {
            inst = this;
        }

        public void StartFromController() {
            GS.GameSparksAvailable += SetOnGSAvailable;
            var gameSparkGo = Instantiate(Resources.Load("Controllers/GameSparksUnity")) as GameObject;
            gameSparkGo.name = "GameSparksUnity";
            GameSparks.Api.Messages.ScriptMessage.Listener += ServerScriptMessageResponse;
        }

        public void OnDisable() {
            Debug.Log("-= ServerResponse");
            GameSparks.Api.Messages.ScriptMessage.Listener -= ServerScriptMessageResponse;
        }

        public void SetOnGSAvailable (bool var) {
            onGSAvailable = var;
        }

        public bool GetOnAuthorizationAvailable () {
            return onAuthorizationAvailable;
        }

        public bool GetOnGSAvailable () {
            return onGSAvailable;
        }

        public void Registration (string displayName) {
            if(!onAuthorizationAvailable) {
                StartCoroutine(IEnumDeviceRegistration(displayName));
            } else {
                GSAPI.CallbackRegistrationDevice(responsesUserId);
            }
        }

        public IEnumerator IEnumDeviceRegistration (string displayName) {
            if(displayName == "") {
                displayName = SystemInfo.deviceName;
            }
            while(!GetOnGSAvailable()) {
                yield return null;
            }

            new GameSparks.Api.Requests.DeviceAuthenticationRequest().SetDisplayName(displayName).Send((responses) => {
                if(!responses.HasErrors) {
                    onAuthorizationAvailable = true;
                    responsesUserId = responses.UserId;
                    GSAPI.CallbackRegistrationDevice(responses.UserId);
                    Debug.Log(string.Format("Device Authenticated... [userId: {0}, authToken: {1}]", responses.UserId, responses.AuthToken));

                } else {
                    onAuthorizationAvailable = false;
                    GSAPI.CallbackRegistrationDevice("");
                }
            });
        }

        public void ServerScriptMessageResponse (ScriptMessage _message) {
            if (GP.showLogs) {
                Debug.Log(_message.JSONString);
            }

            var request = JsonConvert.DeserializeObject<RequestData>(_message.JSONString);
            if (request == null) {
                Debug.Log(" request = NULL!");
                return;
            }
            var responseList = request.data.callback.Split(new[] { "##" }, StringSplitOptions.None);
            if(responseList.Length != 2) {
                //Debug.Log("responseList.Length != 2");
            } else {
                switch(responseList[0]) {
                    case ConstTypeRequest.connectPlayer:
                        if(bool.Parse(responseList[1])) {
                            GSAPI.actionConnectPlayer(true);
                        }
                        break;
                    case ConstTypeRequest.sendDataFromClient:
                        GSAPI.CallbackSendDataFromClient(responseList[1]);
                        break;
                    case ConstTypeRequest.setNewMaze:
                        GSAPI.CallbackBoolSetNewMaze(responseList[1]);
                        break;
                    case ConstTypeRequest.getCountPlayers:
                        GSAPI.CallbackStrGetCountPlayers(responseList[1]);
                        break;
                    case ConstTypeRequest.getMaze:
                        GSAPI.actionGetMaze(responseList[1]);
                        break;
                    case ConstTypeRequest.getPlayersInfo:
                        GSAPI.actionGetPlayersInfo(responseList[1]);
                        break;

                    case ConstTypeRequest.leavePlayer:
                        GSAPI.actionPlayerLeave(responseList[1]);
                        break;
                    case ConstTypeRequest.findRoom:
                        var data = JsonConvert.DeserializeObject<InfoRoom>(responseList[1]);
                        GSAPI.actionFindRoom(data);
                        break;



                }
            }
        }


        public void ServerRequest (InfoToServer infoToServer) {
            if (GP.showLogs) {
                Debug.Log(">===>" + infoToServer.typeRequest + " strData = " + infoToServer.strData + " strData2 = " + infoToServer.idRoom);
            }
           
            switch(infoToServer.typeRequest) {
                case ConstTypeRequest.connectPlayer:
                case ConstTypeRequest.getCountPlayers:
                case ConstTypeRequest.getMaze:
                case ConstTypeRequest.getPlayersInfo:
                case ConstTypeRequest.leavePlayer:
                    new GameSparks.Api.Requests.LogEventRequest().SetEventKey(infoToServer.typeRequest)
                        .SetEventAttribute("SC_NameEvent", infoToServer.typeRequest)
                        .Send((response) => {
                            if(response.HasErrors) {
                                Debug.LogError("RBT| Request Error \n" + response.Errors.JSON);
                            }
                        });
                    break;
                case ConstTypeRequest.findRoom:
                case ConstTypeRequest.setNewMaze:
                case ConstTypeRequest.setInfoCellMazePlayer:
                case ConstTypeRequest.setInfoChatPlayer:
                    new GameSparks.Api.Requests.LogEventRequest().SetEventKey(infoToServer.typeRequest)
                        .SetEventAttribute("SC_NameEvent", infoToServer.typeRequest)
                        .SetEventAttribute("SC_Data", infoToServer.strData)
                        .Send((response) => {
                            if(response.HasErrors) {
                                Debug.LogError("Request Error \n" + response.Errors.JSON);
                            }
                        });
                    break;
                case ConstTypeRequest.sendDataFromClient:
                    new GameSparks.Api.Requests.LogEventRequest().SetEventKey(infoToServer.typeRequest)
                        .SetEventAttribute("SC_NameEvent", infoToServer.typeRequest)
                        .SetEventAttribute("SC_Data", infoToServer.strData)
                        .SetEventAttribute("SC_Data2", infoToServer.idRoom)
                        .Send((response) => {
                            if(response.HasErrors) {
                                Debug.LogError("Request Error \n" + response.Errors.JSON);
                            }
                        });
                    break;
            }
        }
    }
}