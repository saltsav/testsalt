﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Test.Scripts {
    public class DragOnWoldPos: MonoBehaviour, IPointerDownHandler, IPointerClickHandler {
        public Vector3 posCursor;
        [ReadOnly] public bool isDown;
        [ReadOnly] public bool isMoveCard;
        [ReadOnly] public bool dontOpen;
        private float timeDown = 0.1f;
        public float currentTimeDown;

        public void Update () {
           /* if(player.playerId != GameInfo.inst.myID) {
                return;
            }*/
            if(isMoveCard) {
                var flag = false;
                if(Input.touchSupported) {
                    if(Input.touchCount == 1) {
                        posCursor = Input.mousePosition;
                        flag = true;
                    }
                } else {
                    if(Input.GetKey(KeyCode.Mouse0)) {
                        posCursor = Input.mousePosition;
                        flag = true;
                    }
                }
                if(flag) {
                    transform.localPosition = Camera.main.ScreenToWorldPoint(posCursor) + Vector3.forward * 20;
                    dontOpen = true;
                }

                currentTimeDown = 0;

                var flagOff = false;
                if(Input.touchSupported) {
                    if(Input.touchCount == 0) {
                        flagOff = true;
                    }
                } else {
                    if(!Input.GetKey(KeyCode.Mouse0)) {
                        flagOff = true;
                    }
                }
                if(flagOff) {
                    OnPointerUpAfterMove();
                }
            } else {
                if(isDown) {
                    currentTimeDown += Time.deltaTime;
                    if(currentTimeDown > timeDown) {
                        currentTimeDown = 0;
                        OnBeginDragAfterDown();
                    }
                }
            }
        }

        public void OnPointerClick (PointerEventData eventData) {
            isDown = false;
            currentTimeDown = 0;

            if(dontOpen) {
                dontOpen = false;
                return;
            }
        }

        public void OnPointerUpAfterMove () {
            dontOpen = true;
            isDown = false;
            isMoveCard = false;
            dontOpen = false;
        }

        public void OnPointerDown (PointerEventData eventData) {
            isDown = true;
        }

        public void OnBeginDragAfterDown () {
            isMoveCard = true;
        }
    }
}