﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Project.Scripts {
    public enum TypeCell {
        none,
        wall,
        road,
    }

    public class GenerationMaze: MonoBehaviour {
        public static GenerationMaze inst;
        private int constHeight;
        private int constWidth;
        public CellMaze[,] mazeArray;
        public float factor = 0.99f;

        public ViewMaze viewMaze;
        //public ViewMaze viewMazeFake;

        [TextArea(10,10)]
        public string arrayStr;

        private void Awake () {
            constHeight = GP.sizeMaze;
            constWidth = GP.sizeMaze;
            inst = this;
        }

        public void StartFromController (bool createNewMaze) {
            if (createNewMaze) {
                GenerationNewMaze();
            } else {
                GenerationMazeFromServer();
            }
        }

        [EasyButtons.Button]
        public void GenerationMazeFromServer () {
            GSAPI.actionGetMaze += (str) => {
                arrayStr = str;
                GSAPI.SetNewMaze(arrayStr);
                CreateMazeFromArrayStr();
                //var mid = viewMaze.GetCellOnViewByIandJ((int)(constHeight * 0.5), (int)(constHeight * 0.5));
                //CameraController.inst.SetPositionForCamera(mid.transform.position);
            };
            GSAPI.GetMaze();
           
        }

        public void GenerationNewMaze () {
            GenFirstBase();
            GenerationMazeFull();
            AddHoles();
            arrayStr = GetStrMaze();
            GSAPI.SetNewMaze(arrayStr);
            //var mid = viewMaze.GetCellOnViewByIandJ((int) (constHeight * 0.5), (int) (constHeight * 0.5));
            //CameraController.inst.SetPositionForCamera(mid.transform.position);
        }

        [EasyButtons.Button]
        public void GenFirstBase () {
            mazeArray = new CellMaze[constWidth, constHeight];
            GenerationDefault();
            //viewMaze.ShowThisMaze(mazeArray);
        }

        public void GenerationDefault () {
            for(int i = 0; i < constHeight; i++) {
                for(int j = 0; j < constWidth; j++) {
                    mazeArray[i, j] = new CellMaze {j = j, i = i};
                    if((i % 2 != 0 && j % 2 != 0) && (i < constHeight - 1 && j < constWidth - 1)) {
                        mazeArray[i, j].typeCell = TypeCell.road;
                    } else {
                        mazeArray[i, j].typeCell = TypeCell.wall;
                    }
                }
            }
        }

        public void GenerationMazeFull () {
            var startI = 1;
            var startJ = 1;
            var currentCell = mazeArray[startI, startJ];
            var index = 0;
            do {
                var flag = false;
                do {
                    flag = true;
                    SetNeighbours(currentCell);
                    currentCell.isVisited = true;
                    if(currentCell.cellsNeighbours.Count == 0) {
                        flag = false;
                    } else {
                        var randomIndex = UnityEngine.Random.Range(0, currentCell.cellsNeighbours.Count);
                        DestroyWall(currentCell, currentCell.cellsNeighbours[randomIndex], index);
                        var newCurrentCell = currentCell.cellsNeighbours[randomIndex];
                        currentCell.cellsNeighbours.RemoveAt(randomIndex);
                        currentCell = newCurrentCell;
                    }
                } while(flag);
                currentCell = GetNextCellMaze();
                if(currentCell == null) {
                    break;
                }

                index++;
            } while(HaveCellWithDontVisitNeighbours());
        }

        public void AddHoles () {
            for(int i = 1; i < constHeight - 1; i++) {
                for(int j = 1; j < constWidth - 1; j++) {
                    if(mazeArray[i, j].typeCell == TypeCell.wall) {
                        var k = 0;
                        if(mazeArray[i + 1, j].typeCell == TypeCell.road) {
                            k++;
                        }
                        if(mazeArray[i - 1, j].typeCell == TypeCell.road) {
                            k++;
                        }
                        if(mazeArray[i, j + 1].typeCell == TypeCell.road) {
                            k++;
                        }
                        if(mazeArray[i, j - 1].typeCell == TypeCell.road) {
                            k++;
                        }

                        if(k <= 2) {
                            if((mazeArray[i + 1, j].typeCell == TypeCell.road && mazeArray[i - 1, j].typeCell == TypeCell.road)
                                || (mazeArray[i, j + 1].typeCell == TypeCell.road && mazeArray[i, j - 1].typeCell == TypeCell.road)) {
                                if(UnityEngine.Random.value > factor) {
                                    mazeArray[i, j].typeCell = TypeCell.road;
                                    //viewMaze.UpdateMaze(mazeArray[i, j]);
                                }
                            }
                        }

                    }
                }
            }
        }

        public bool HaveCellWithDontVisitNeighbours () {
            for(int i = 1; i < constHeight; i = i + 2) {
                for(int j = 1; j < constWidth; j = j + 2) {
                    if(mazeArray[i, j].cellsNeighbours.Count != 0) {
                        return true;
                    }
                }
            }

            return false;
        }

        public CellMaze GetNextCellMaze () {
            var listNeighbours = new List<CellMaze>();
            for(var i = 1; i < constHeight; i = i + 2) {
                for(var j = 1; j < constWidth; j = j + 2) {
                    SetNeighbours(mazeArray[i, j]);
                    if(mazeArray[i, j].cellsNeighbours.Count != 0 && mazeArray[i, j].isVisited) {
                        listNeighbours.Add(mazeArray[i, j]);
                        //return mazeArray[i, j];
                    }
                }
            }

            if (listNeighbours.Count > 0) {
                return listNeighbours[UnityEngine.Random.Range(0, listNeighbours.Count - 1)];
            }

            return null;
        }

        public void DestroyWall (CellMaze first, CellMaze second, int index) {
            //viewMaze.UpdateMaze(first);
            //viewMaze.UpdateMaze(second);
            if(first.i == second.i) {
                if(first.j < second.j) {
                    mazeArray[first.i, first.j + 1].typeCell = TypeCell.road;
                    //viewMaze.UpdateMaze(mazeArray[first.i, first.j + 1]);
                    //viewMaze.SetAroundWallMaterial(first.i, first.j + 1, index);
                } else {
                    mazeArray[first.i, first.j - 1].typeCell = TypeCell.road;
                    //viewMaze.UpdateMaze(mazeArray[first.i, first.j - 1]);
                    //viewMaze.SetAroundWallMaterial(first.i, first.j - 1, index);
                }
            }
            if(first.j == second.j) {
                if(first.i < second.i) {
                    mazeArray[first.i + 1, first.j].typeCell = TypeCell.road;
                    //viewMaze.UpdateMaze(mazeArray[first.i + 1, first.j]);
                    //viewMaze.SetAroundWallMaterial(first.i + 1, first.j, index);
                } else {
                    mazeArray[first.i - 1, first.j].typeCell = TypeCell.road;
                    //viewMaze.UpdateMaze(mazeArray[first.i - 1, first.j]);
                    //viewMaze.SetAroundWallMaterial(first.i - 1, first.j, index);
                }
            }
        }

        public void SetNeighbours (CellMaze cellMaze) {
            cellMaze.cellsNeighbours.Clear();
            if(cellMaze.i + 2 < constHeight && !mazeArray[cellMaze.i + 2, cellMaze.j].isVisited) {
                cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.i + 2, cellMaze.j]);
            }
            if(cellMaze.i - 2 > 0 && !mazeArray[cellMaze.i - 2, cellMaze.j].isVisited) {
                cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.i - 2, cellMaze.j]);
            }
            if(cellMaze.j + 2 < constWidth && !mazeArray[cellMaze.i, cellMaze.j + 2].isVisited) {
                cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.i, cellMaze.j + 2]);
            }
            if(cellMaze.j - 2 > 0 && !mazeArray[cellMaze.i, cellMaze.j - 2].isVisited) {
                cellMaze.cellsNeighbours.Add(mazeArray[cellMaze.i, cellMaze.j - 2]);
            }
        }

        public CellMaze GetEmptyCellMaze() {
            var listNeighbours = new List<CellMaze>();
            for(var i = 1; i < constHeight; i = i + 2) {
                for(var j = 1; j < constWidth; j = j + 2) {
                    SetNeighbours(mazeArray[i, j]);
                    if(mazeArray[i, j].typeCell == TypeCell.road) {
                        listNeighbours.Add(mazeArray[i, j]);
                    }
                }
            }

            if(listNeighbours.Count > 0) {
                return listNeighbours[UnityEngine.Random.Range(0, listNeighbours.Count - 1)];
            }

            return null;
        }

        /*public CellMaze GetCellMazeBySwipeDirection (SwipeDirection swipeDirection, CellMaze currentCellMaze) {
            CellMaze finalCellMaze = null;
            switch(swipeDirection) {
                case SwipeDirection.Up:
                    if(mazeArray[currentCellMaze.i, currentCellMaze.j + 1].typeCell == TypeCell.road) {
                        finalCellMaze = mazeArray[currentCellMaze.i, currentCellMaze.j + 1];
                    }
                    break;
                case SwipeDirection.Down:
                    if(mazeArray[currentCellMaze.i, currentCellMaze.j - 1].typeCell == TypeCell.road) {
                        finalCellMaze = mazeArray[currentCellMaze.i, currentCellMaze.j - 1];
                    }
                    break;
                case SwipeDirection.Left:
                    if(mazeArray[currentCellMaze.i - 1, currentCellMaze.j].typeCell == TypeCell.road) {
                        finalCellMaze = mazeArray[currentCellMaze.i - 1, currentCellMaze.j];
                    }
                    break;
                case SwipeDirection.Right:
                    if(mazeArray[currentCellMaze.i + 1, currentCellMaze.j].typeCell == TypeCell.road) {
                        finalCellMaze = mazeArray[currentCellMaze.i + 1, currentCellMaze.j];
                    }
                    break;
            }

            return finalCellMaze;
        }*/


        public string GetStrMaze() {
            var str = "";
            for (var i = 0; i < constHeight; i++) {
                for (var j = 0; j < constWidth; j++) {
                    str += mazeArray[i, j].GetTypeInStr();
                }
            }

            return str;
        }

        public void CreateMazeFromArrayStr() {
            MainUI.inst.SetActiveBlockModal(false);
            int ii = 0;
            int jj = 0;

            var newMazeArray = new CellMaze[constWidth, constHeight];
            for (var k = 0; k < arrayStr.Length; k++) {
                //Debug.Log("k=" + k + " ii=" + ii + " jj=" + jj);
                newMazeArray[ii, jj] = new CellMaze();
                newMazeArray[ii, jj].i = ii;
                newMazeArray[ii, jj].j = jj;
                newMazeArray[ii, jj].typeCell = arrayStr[k].ToString() == "1" ? TypeCell.wall : TypeCell.road;
                jj++;
                if (jj == constHeight) {
                    jj = 0;
                    ii++;
                }
                
            }
            //viewMaze.ShowThisMaze(newMazeArray);
            mazeArray = newMazeArray;
            //viewMazeFake.ShowThisMaze(newMazeArray);
        }

        public static List<CellMaze> ConvertToListCellMaze(List<WrapperCellMaze> listWrapperCellMaze) {
            var listCellMaze = new List<CellMaze>();
            foreach(var wrapperCellMaze in listWrapperCellMaze) {
                listCellMaze.Add(inst.mazeArray[wrapperCellMaze.i, wrapperCellMaze.j]);
            }
            return listCellMaze;
        }


        public static List<WrapperCellMaze> ConvertToListWrapperCellMaze (List<CellMaze> listCellMaze) {
            var listWrapperCellMaze = new List<WrapperCellMaze>();
            foreach (var cellMaze in listCellMaze) {
                listWrapperCellMaze.Add(new WrapperCellMaze(){i = cellMaze.i, j = cellMaze.j});
            }
            return listWrapperCellMaze;
        }

    }

    public class CellMaze {
        public bool isVisited;
        public int i;
        public int j;
        public TypeCell typeCell = TypeCell.none;
        public List<CellMaze> cellsNeighbours = new List<CellMaze>();

        public List<CellMaze> cellsRoundRoad = new List<CellMaze>();
        public CellMaze beforRoad;

        public int numberMassPath = 0;

        public int matIndex;

        public string GetTypeInStr () {
            switch(typeCell) {
                case TypeCell.none:
                    return "!";
                case TypeCell.wall:
                    return "1";
                case TypeCell.road:
                    return "0";
            }

            return "*";
        }
    }

    [Serializable]
    public class WrapperCellMaze {
        public int i;
        public int j;
    }

}