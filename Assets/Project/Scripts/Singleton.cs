﻿using System;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour {
        private static T _inst;
        private static object _lock = new object();

        public static T inst {
            get {
                if (_inst != null) {
                    return _inst;
                }

                if (applicationIsQuitting) {
                    Debug.LogWarning("applicationIsQuitting " + applicationIsQuitting + " T= " + typeof(T));
                    return null;
                }

                lock (_lock) {
                    if (!_inst) {
                        var baseController = SpawnController.inst.InstController<T>(typeof(T).Name.ToString());
                        _inst = baseController;
                    }
                }

                return _inst;
            }
            set { _inst = value; }
        }

        public Action actionCallSingltone;

        private static bool applicationIsQuitting = false;

        public void Call(Action action = null) {
            if (action != null) {
                actionCallSingltone = action;
            }
        }

        public virtual void Awake() {
            if (_inst != null) {
                Destroy(gameObject);
            } else {
                DontDestroyOnLoad(gameObject);
                _inst = gameObject.GetComponent<T>();
            }
        }

        public virtual void OnDestroy() {
            applicationIsQuitting = true;
        }
    }
}