﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class ViewMaze: MonoBehaviour {
        public GameObject cube;
        public GameObject sphere;
        public List<CellOnView> listCellOnView = new List<CellOnView>();
        public Transform parentMaze;

        //public List<Material> listMaterials;

        public void Start() {
           /* foreach (var mat in listMaterials) {
                mat.color = Random.ColorHSV();
            }*/
        }

        [EasyButtons.Button]
        public void Clear () {
            foreach(var listGameObject in listCellOnView) {
                Destroy(listGameObject.gameObject);
            }

            listCellOnView.Clear();
        }

        /*[EasyButtons.Button]
        public void SetupOnEmpty() {
            var wall = Instantiate(sphere);
            var emptyCellMaze = GenerationMaze.inst.GetEmptyCellMaze();
            wall.transform.localPosition = new Vector3(emptyCellMaze.i, emptyCellMaze.j, 0);
        }*/

        public void ShowThisMaze (CellMaze[,] maze) {
            Clear();
            for(int i = 0; i < GP.sizeMaze; i++) {
                for(int j = 0; j < GP.sizeMaze; j++) {
                    if(maze[i, j].typeCell == TypeCell.wall) {
                        SetupWall(maze, i, j);
                    }

                    if (maze[i, j].typeCell == TypeCell.road) {
                        SetupRoad(maze, i, j);
                    }
                   
                }
            }
        }

        public void SetupWall(CellMaze[,] maze, int i, int j) {
            var wall = Instantiate(cube);
            wall.transform.SetParent(parentMaze);
            wall.transform.localPosition = new Vector3(i, j, GP.zMaze);
            var cellOnView = wall.AddComponent<CellOnView>();
            cellOnView.Setup();
            cellOnView.cellMaze = maze[i, j];
            wall.name = "wall_i=" + i + "_j=" + j;
            listCellOnView.Add(cellOnView);
        }

        public void SetupRoad (CellMaze[,] maze, int i, int j) {
            var road = Instantiate(cube);
            road.transform.SetParent(parentMaze);
            road.transform.localScale = Vector3.one;
            road.transform.localPosition = new Vector3(i, j, GP.zMaze);
            var cellOnView = road.AddComponent<CellOnView>();
            cellOnView.gameObject.GetComponent<MeshRenderer>().enabled = false;
            cellOnView.gameObject.GetComponent<Collider>().enabled = true;
            cellOnView.name = "road_i=" + i + "_j=" + j;
            cellOnView.Setup();
            cellOnView.cellMaze = maze[i, j];
            listCellOnView.Add(cellOnView);
        }

        public void UpdateMaze (CellMaze cellMaze) {
            var cellOnView = GetCellOnViewByIandJ(cellMaze.i, cellMaze.j);
            cellOnView.cellMaze = cellMaze;
            if (cellMaze.typeCell == TypeCell.road) {
                //cellOnView.gameObject.SetActive(false);
                cellOnView.gameObject.GetComponent<MeshRenderer>().enabled = false;
                cellOnView.gameObject.GetComponent<Collider>().enabled = true;
                cellOnView.name = "road_i=" + cellMaze.i + "_j=" + cellMaze.j;
            }
        }

        public void SetAroundWallMaterial(int i, int j, int index) {
            return;
            /*while (index > listMaterials.Count) {
                index -= listMaterials.Count;
            }

            for (int k = i-1; k < i+1; k++) {
                for (int l = j-1; l < j+1; l++) {
                    GetCellOnViewByIandJ(k, l).SetMat(listMaterials[index]);
                }

            }*/
            
        }

        public void ResetText () {
            for(int i = 0; i < listCellOnView.Count; i++) {
                listCellOnView[i].textMesh.text = "";
                listCellOnView[i].transform.localScale = Vector3.one;
                if (listCellOnView[i].cellMaze.typeCell == TypeCell.road) {
                    listCellOnView[i].gameObject.GetComponent<MeshRenderer>().enabled = false;
                    listCellOnView[i].gameObject.GetComponent<Collider>().enabled = true;
                }
            }
        }

        public CellOnView GetCellOnViewByIandJ (int _i, int _j) {
            for(int i = 0; i < listCellOnView.Count; i++) {
                if(listCellOnView[i].cellMaze.i == _i && listCellOnView[i].cellMaze.j == _j) {
                    return listCellOnView[i];
                }
            }

            return null;
        }

    }
}