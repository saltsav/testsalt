﻿using System.Collections;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class MainLogic : MonoBehaviour {
        public static MainLogic inst;

        public Coroutine coroWaitConnectToServer;

        public static bool findRoomEnd = false;

        private void Awake() {
            inst = this;
        }

        private void Start() {
            StartFromController();
        }

        public void StartFromController() {
            StopCoroWaitConnectToServer();
            coroWaitConnectToServer = StartCoroutine(IEnumWaitConnectToServer());
        }

        public void StopCoroWaitConnectToServer() {
            if (coroWaitConnectToServer != null) {
                StopCoroutine(coroWaitConnectToServer);
            }

            coroWaitConnectToServer = null;
        }

        public IEnumerator IEnumWaitConnectToServer() {
            MainUI.inst.SetActiveBlockModal(true, TextRepository.connectingToServer);
            GSServer.inst.StartFromController();
            while (!GSServer.inst.GetOnGSAvailable()) {
                yield return null;
            }
            GameInfo.inst.StartFromController();

            while(!GameInfo.inst.isRegistration || !GameInfo.inst.isPlayerConnected) {
                yield return null;
            }
            MainUI.inst.SetActiveBlockModal(true, TextRepository.connectedToServer);
            GenerationMaze.inst.GenerationNewMaze();
            //yield return StartCoroutine(IEnumFindRoom());//salt
            //yield return StartCoroutine(IEnumCreatePlayer(GameInfo.inst.GetIdPlayer()));
            MainUI.inst.SetActiveBlockModal(false);
            MainUI.inst.SetActiveBlockMainMenu(true);
            MainUI.inst.SetActiveBlockGameUI(false);
            CameraController.inst.SetGameCamera(false);
        }

        /*public IEnumerator IEnumFindRoom () {
            GSAPI.CallbackStrGetCountPlayers += (var) => {
                if(var == "0") {
                    GSAPI.GetCountPlayers();
                    return;
                }
                findRoomEnd = true;
                if(var == "1") {
                    MainUI.inst.SetActiveBlockModal(true, TextRepository.createMaze);
                    GenerationMaze.inst.StartFromController(true);
                    
                } else {
                    MainUI.inst.SetActiveBlockModal(true, TextRepository.createMaze);
                    GenerationMaze.inst.StartFromController(false);
                }

                
            };
           
            yield return new WaitForSeconds(1f);
            GSAPI.GetCountPlayers();//salt
            while(true) {
                if(findRoomEnd) {
                    break;
                }

                yield return null;
            }
            yield return new WaitForSeconds(1f);
        }*/

        public IEnumerator IEnumCreatePlayer(string id) {
            PlayerManager.inst.StartFromController(id);
            yield return null;
        }

        public void StartPlayRoom() {
            GenerationMaze.inst.arrayStr = LobbyController.inst.strMaze;
            GenerationMaze.inst.CreateMazeFromArrayStr();
            GenerationMaze.inst.viewMaze.ShowThisMaze(GenerationMaze.inst.mazeArray);
            MainUI.inst.SetActiveBlockMainMenu(false);
            MainUI.inst.SetActiveBlockGameUI(true);
            CameraController.inst.SetGameCamera(true);
            PlayerManager.inst.StartFromController(GameInfo.inst.GetIdPlayer());

            //var mid = GenerationMaze.inst.viewMaze.GetCellOnViewByIandJ((int)(GP.sizeMaze * 0.5), (int)(GP.sizeMaze * 0.5));
            //CameraController.inst.SetPositionForCamera(mid.transform.position);

            //var mid = GenerationMaze.inst.viewMaze.GetCellOnViewByIandJ((int)(GP.sizeMaze * 0.5), (int)(GP.sizeMaze * 0.5));
            CameraController.inst.SetPositionForCamera(PlayerManager.inst.GetPlayer().transform.position);
        }

        public void LeavePlayRoom() {
            PlayerManager.inst.StopFromController();
            MainUI.inst.SetActiveBlockMainMenu(true);
            MainUI.inst.SetActiveBlockGameUI(false);
            CameraController.inst.SetGameCamera(false);
            LobbyController.inst.btnLeaveRoom.onClick.Invoke();
        }
    }


}