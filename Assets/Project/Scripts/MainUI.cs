﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Project.Scripts {
    public class MainUI : MonoBehaviour {
        public static MainUI inst;

        public RectTransform blockMainMenu;
        public RectTransform blockModal;
        public RectTransform blockGameUI;
        public Text textBlockModal;

        public Button leaveRoom;

        private void Awake () {
            inst = this;
        }

        public void Start() {
            leaveRoom.onClick.RemoveAllListeners();
            leaveRoom.onClick.AddListener(() => {
                MainLogic.inst.LeavePlayRoom();
            });
        }

        public void SetActiveBlockMainMenu (bool var) {
            blockMainMenu.gameObject.SetActive(var);
        }

        public void SetActiveBlockGameUI (bool var) {
            blockGameUI.gameObject.SetActive(var);
        }

        public void SetActiveBlockModal (bool var, string str = "" ) {
            textBlockModal.text = str;
            blockModal.gameObject.SetActive(var);
        }
    }
}