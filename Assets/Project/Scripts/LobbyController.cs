﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Project.Scripts {
    public class LobbyController : MonoBehaviour {
        public static LobbyController inst;
        public Button btnFindRoom;
        public Button btnLeaveRoom;
        public Text currentNumberRoom;
        public string numberRoom;
        public string strMaze;

        public void Awake() {
            inst = this;
        }

        public void Start() {
            btnLeaveRoom.interactable = false;
            btnFindRoom.onClick.RemoveAllListeners();
            btnFindRoom.onClick.AddListener(() => {
                GSAPI.actionFindRoom += CallbackActionFindRoom;
                GSAPI.FindRoom(GenerationMaze.inst.arrayStr);
                btnFindRoom.interactable = false;
                btnLeaveRoom.interactable = true;
            });

            btnLeaveRoom.onClick.RemoveAllListeners();
            btnLeaveRoom.onClick.AddListener(() => {
                GSAPI.actionFindRoom -= CallbackActionFindRoom;
                btnFindRoom.interactable = true;
                btnLeaveRoom.interactable = false;
                GSAPI.PlayerLeave();
                strMaze = "";
                numberRoom = "";
                currentNumberRoom.text = numberRoom;
            });
            
        }


        public void CallbackActionFindRoom(InfoRoom infoRoom) {
            strMaze = infoRoom.mazeRoom;
            numberRoom = infoRoom.numberRoom;
            currentNumberRoom.text = numberRoom;

           
            if (infoRoom.isRoomFull) {
                Debug.LogError("START");
                MainLogic.inst.StartPlayRoom();
                GSAPI.actionFindRoom -= CallbackActionFindRoom;
            }

        }
    }
}
