﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Project.Scripts {
    public enum FirstArrow {
        none,
        up,
        left,
        down,
        right

    }

    public enum SecondArrow {
        none,
        upLeft,
        upRight,
        downLeft,
        downRight

    }

    public enum FinalArrow {
        none,
        zone1,
        zone2,
        zone3,
        zone4,
        zone5,
        zone6,
        zone7,
        zone8

    }

    public class JoystickController : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {
        public static JoystickController inst;
        public Vector2 currentVector2;
        public Vector2 downVector2;
        public Vector2 upVector2;
        public Vector2 finalStickPosV2;

        public RectTransform backStick;
        public RectTransform stick;
        /*public float x;
        public float y;*/

        public float r;

        public FirstArrow firstArrow = FirstArrow.none;
        public SecondArrow secondArrow = SecondArrow.none;
        public FinalArrow finalArrow = FinalArrow.none;
        public float dis;

        public void Awake() {
            inst = this;
        }

        public void Start() {
            SetActiveStick(false);
        }

        public void SetActiveStick(bool var) {
            backStick.gameObject.SetActive(var);
            stick.gameObject.SetActive(var);
        }

        public void BackStickSetPosition (Vector2 v2) {
            backStick.position = v2;
        }

        public void StickSetPosition () {
            stick.position = CalculateFinalStickPos();
            firstArrow = CalculateMainArrow();
            secondArrow = CalculateSecondArrow();
            finalArrow = CalculateFinalArrow();
        }

        public FinalArrow CalculateFinalArrow() {
            switch (firstArrow) {
                case FirstArrow.none:
                    return FinalArrow.none;
                case FirstArrow.up:
                    switch (secondArrow) {
                        case SecondArrow.upLeft:
                            return FinalArrow.zone8;
                        case SecondArrow.upRight:
                            return FinalArrow.zone1;
                    }
                    break;
                case FirstArrow.left:
                    switch(secondArrow) {
                        case SecondArrow.upLeft:
                            return FinalArrow.zone7;
                        case SecondArrow.downLeft:
                            return FinalArrow.zone6;
                    }
                    break;
                case FirstArrow.down:
                    switch(secondArrow) {
                        case SecondArrow.downLeft:
                            return FinalArrow.zone5;
                        case SecondArrow.downRight:
                            return FinalArrow.zone4;
                    }
                    break;
                case FirstArrow.right:
                    switch(secondArrow) {
                        case SecondArrow.upRight:
                            return FinalArrow.zone2;
                        case SecondArrow.downRight:
                            return FinalArrow.zone3;
                    }
                    break;
            }

            return FinalArrow.none;
        }

        public Vector2 CalculateFinalStickPos () {
            var x1 = Mathf.Abs(downVector2.x - currentVector2.x);
            var y1 = Mathf.Abs(downVector2.y - currentVector2.y);
            dis = x1 * x1 + y1 * y1;
            var r2 = r * r;

            if(dis < r2) {
                finalStickPosV2 = currentVector2;
            } else {
                var xm = (r * x1) / (Mathf.Sqrt(dis));
                var ym = (r * y1) / (Mathf.Sqrt(dis));
                if (currentVector2.x > downVector2.x) {

                } else {
                    xm = -1 * xm;
                }
                if(currentVector2.y > downVector2.y) {

                } else {
                    ym = -1 * ym;
                }
                finalStickPosV2.x = xm + downVector2.x;
                finalStickPosV2.y = ym + downVector2.y;
            }
            return finalStickPosV2;
        }

        public FirstArrow CalculateMainArrow() {
            if (dis < 500) {
                return FirstArrow.none;
            }
            var x = currentVector2.x - downVector2.x;
            var y = currentVector2.y - downVector2.y;

            if (y > 0) {
                if (x > 0) {
                    if (Mathf.Abs(x) > Mathf.Abs(y)) {
                        return FirstArrow.right;
                    } else {
                        return FirstArrow.up;
                    }
                } else {
                    if(Mathf.Abs(x) > Mathf.Abs(y)) {
                        return FirstArrow.left;
                    } else {
                        return FirstArrow.up;
                    }
                }
            } else {
                if(x > 0) {
                    if(Mathf.Abs(x) > Mathf.Abs(y)) {
                        return FirstArrow.right;
                    } else {
                        return FirstArrow.down;
                    }
                } else {
                    if(Mathf.Abs(x) > Mathf.Abs(y)) {
                        return FirstArrow.left;
                    } else {
                        return FirstArrow.down;
                    }
                }
            }
        }

        public SecondArrow CalculateSecondArrow () {
            if(dis < 500) {
                return SecondArrow.none;
            }
            var x = currentVector2.x - downVector2.x;
            var y = currentVector2.y - downVector2.y;

            if(y > 0) {
                if(x > 0) {
                    return SecondArrow.upRight;
                } else {
                    return SecondArrow.upLeft;
                }
            } else {
                if(x > 0) {
                    return SecondArrow.downRight;
                } else {
                    return SecondArrow.downLeft;
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData) {
            downVector2 = eventData.position;
            currentVector2 = eventData.position;
            BackStickSetPosition(eventData.position);
            StickSetPosition();
            SetActiveStick(true);
        }

        public void OnDrag(PointerEventData eventData) {
            currentVector2 = eventData.position;
            StickSetPosition();
        }

        public void OnPointerUp(PointerEventData eventData) {
            upVector2 = eventData.position;
            firstArrow = FirstArrow.none;
            secondArrow = SecondArrow.none;
            finalArrow = FinalArrow.none;
            SetActiveStick(false);
        }

        /*public Vector2 ConvertScreenToCanvas(Vector2 v2) {
            x = (v2.x / Screen.width) * 1440;
            y = (v2.y / Screen.height) * 2560;
            return new Vector2(x, y);
        }*/
    }
}
