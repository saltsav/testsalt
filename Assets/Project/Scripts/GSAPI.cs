﻿using System;
using UnityEngine;

namespace Assets.Project.Scripts {
    public class ConstTypeRequest {
        public const string connectPlayer = "connectPlayer";
        public const string sendDataFromClient = "sendDataFromClient";
        public const string setNewMaze = "setNewMaze";
        public const string getMaze = "getMaze";
        public const string getCountPlayers = "getCountPlayers";
        public const string setInfoCellMazePlayer = "setInfoCellMazePlayer";
        public const string getPlayersInfo = "getPlayersInfo";
        public const string leavePlayer = "leavePlayer";
        public const string setInfoChatPlayer = "setInfoChatPlayer";
        public const string findRoom = "findRoom";
    }

    public class GSAPI : MonoBehaviour {
        public static GSAPI inst;

        private void Awake () {
            inst = this;
        }

        public static void RegistrationDevice (string displayName) {
            GSServer.inst.Registration(displayName);
        }
        private static Action<string> statusRegistration { get; set; }
        public static Action<string> CallbackRegistrationDevice {
            get {
                return statusRegistration;
            }
            set {
                statusRegistration = value;
            }
        }
        
        public static void ConnectPlayer () {
            var infoToServer = new InfoToServer();
            infoToServer.typeRequest = ConstTypeRequest.connectPlayer;
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<bool> _actionConnectPlayer { get; set; }
        public static Action<bool> actionConnectPlayer {
            get {
                return _actionConnectPlayer;
            }
            set {
                _actionConnectPlayer = value;
            }
        }
        
        public static void SendDataFromClient (string data, string idRoom) {
            var infoToServer = new InfoToServer {
                typeRequest = ConstTypeRequest.sendDataFromClient,
                strData = data,
                idRoom = idRoom
            };
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<string> dataFromClient { get; set; }
        public static Action<string> CallbackSendDataFromClient {
            get {
                return dataFromClient;
            }
            set {
                dataFromClient = value;
            }
        }
        
        public static void SetNewMaze (string data) {
            var infoToServer = new InfoToServer {
                typeRequest = ConstTypeRequest.setNewMaze,
                strData = data
            };
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<string> boolSetNewMaze { get; set; }
        public static Action<string> CallbackBoolSetNewMaze {
            get {
                return boolSetNewMaze;
            }
            set {
                boolSetNewMaze = value;
            }
        }

        public static void GetCountPlayers () {
            var infoToServer = new InfoToServer();
            infoToServer.typeRequest = ConstTypeRequest.getCountPlayers;
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<string> strGetCountPlayers { get; set; }
        public static Action<string> CallbackStrGetCountPlayers {
            get {
                return strGetCountPlayers;
            }
            set {
                strGetCountPlayers = value;
            }
        }

        [EasyButtons.Button]
        public static void GetPlayersInfo () {
            var infoToServer = new InfoToServer();
            infoToServer.typeRequest = ConstTypeRequest.getPlayersInfo;
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<string> _actionGetPlayersInfo { get; set; }
        public static Action<string> actionGetPlayersInfo {
            get {
                return _actionGetPlayersInfo;
            }
            set {
                _actionGetPlayersInfo = value;
            }
        }
        

        public static void GetMaze () {
            var infoToServer = new InfoToServer {
                typeRequest = ConstTypeRequest.getMaze
            };
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<string> _actionGetMaze { get; set; }
        public static Action<string> actionGetMaze {
            get {
                return _actionGetMaze;
            }
            set {
                _actionGetMaze = value;
            }
        }


        public static void SetInfoCellMazePlayer (string data) {
            var infoToServer = new InfoToServer {
                typeRequest = ConstTypeRequest.setInfoCellMazePlayer,
                strData = data
            };
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<bool> _actionSetInfoCellMazePlayer { get; set; }
        public static Action<bool> actionSetInfoCellMazePlayer {
            get {
                return _actionSetInfoCellMazePlayer;
            }
            set {
                _actionSetInfoCellMazePlayer = value;
            }
        }

        public static void SetInfoChatPlayer (string data) {
            var infoToServer = new InfoToServer {
                typeRequest = ConstTypeRequest.setInfoChatPlayer,
                strData = data
            };
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<bool> _actionSetInfoChatPlayer { get; set; }
        public static Action<bool> actionSetInfoChatPlayer {
            get {
                return _actionSetInfoChatPlayer;
            }
            set {
                _actionSetInfoChatPlayer = value;
            }
        }


        public static void PlayerLeave () {
            var infoToServer = new InfoToServer {
                typeRequest = ConstTypeRequest.leavePlayer,
            };
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<string> _actionPlayerLeave { get; set; }
        public static Action<string> actionPlayerLeave {
            get {
                return _actionPlayerLeave;
            }
            set {
                _actionPlayerLeave = value;
            }
        }


        //[EasyButtons.Button]
        public static void FindRoom (string data) {
            var infoToServer = new InfoToServer {
                typeRequest = ConstTypeRequest.findRoom,
                strData = data,
            };
            GSServer.inst.ServerRequest(infoToServer);
        }
        private static Action<InfoRoom> _actionFindRoom { get; set; }
        public static Action<InfoRoom> actionFindRoom {
            get {
                return _actionFindRoom;
            }
            set {
                _actionFindRoom = value;
            }
        }
        
    }
}